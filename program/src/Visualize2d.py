#!/usr/bin/python3

import sys
import matplotlib.pyplot as plt


def visualize(f_name):
    x = []
    y = []
    with open(f_name, 'r') as f:
        for line in f:
            coord = [int(s) for s in line.split(',')]
            x.append(coord[0])
            y.append(coord[1])
            plt.scatter(x, y)
    plt.show()

if __name__ == '__main__':
    if len(sys.argv) == 2:
        visualize(sys.argv[-1])
