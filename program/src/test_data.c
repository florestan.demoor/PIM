#include <stdio.h>
#include "data.h"

#define N 3
#define M 2

int main() {
  uint i,j;
  data_t data = init_data(N, M);
  print_data(&data);
  for(i = 0; i < N; ++i)
    for(j = 0; j < M; ++j)
      set_data(&data, i, j, i*M + j);
  printf("\n");
  print_data(&data);

  printf("\nLine 2 :\n");
  int *line = get_line(&data, 1); 
  for(i = 0; i < M; ++i) {
       printf("%d ", *(line + i));
  }
  printf("\n");
  
  free_data(&data);
  return 0;
}
