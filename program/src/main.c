#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <inttypes.h>
#include <time.h>

#include "dpu.h"
#include "tools.c"
#include "IO.h"
#include "data.h"
#include "conf.h"
#include "init.h"

/* Path to the program loaded on the DPU. */
static const char dpu_program[] = "./kmeans.bin";
/* Path of the configuration file */
#define CONF_PATH "./config.conf"
/* Verbose option: if true steps are printed on the screen */
//#define VERBOSE
//#define DATASET

#define BLOCK_SIZE 256

#define ERR           1
#define ERR_DPU_ALLOC 2
#define ERR_DPU       3
#define OUT_OF_MEMORY 4

/** Return the clock timer value */
double wtime(void) {
	double value;
	value = (double) clock() / (double) CLOCKS_PER_SEC;
	return value;
}


int main(void) {
	double timing           = 0;
	double timing2          = 0;
	double total_time       = 0;
	double init_dpu_time    = 0;
	double send_points_time = 0;
	double IO_time          = 0;
	double waiting_time     = 0;
	double computation_time = 0;
	double load_dpu_time    = 0;
	double get_dpu_time     = 0;
	double boot_dpu_time    = 0;
	double init_c           = 0;
	double update_c         = 0;

	total_time = wtime();

	uint id_point, id_dim, id_k, id_dpu;
	conf_t config = get_conf(CONF_PATH);

	IO_time = wtime();
	data_t DATA = load_data(config.path);
	IO_time = wtime() - IO_time;

#ifdef VERBOSE
	printf("\n|*** Configuration ***|\n");
	print_conf(&config);
#endif
#ifdef DATASET
	printf("\n|*** Dataset ***|\n");
	print_data(&DATA);
#endif

	init_dpu_time = wtime();
	// Kmeans variables
	const uint N        = DATA.N;
	const uint D        = DATA.M;
	const uint K        = config.nb_clusters;
	const uint N_DPU    = config.nb_dpus;
	const uint N_THREAD = config.nb_threads;
	const char* DEST    = config.dest;

	uint64_t *durations = (uint64_t *) malloc(N_DPU * sizeof(uint64_t));
	uint64_t time = 0;

	get_dpu_time = wtime();
	// DPUs allocation
	dpu_t *DPUs = (dpu_t *) malloc(N_DPU * sizeof(dpu_t));
	for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
#ifndef CAS
		/* struct dpu_param params = {
			.type = HSIM,
			.profile = "profiling",
			.on_boot = NULL
		};
		DPUs[id_dpu] = dpu_alloc(&params); */
		DPUs[id_dpu] = dpu_get_with_type(HSIM);
#endif
#ifdef CAS
		DPUs[id_dpu] = dpu_get_with_type(CYCLE_ACCURATE_SIMULATOR);
#endif
		if (DPUs[id_dpu] == NO_DPU_AVAILABLE) {
			fprintf(stderr, "Failed to allocate DPU!\n");
			exit(ERR_DPU_ALLOC);
		}
	}
	get_dpu_time = wtime() - get_dpu_time;

	// Number of points per dpu
	int *nb_points_per_dpu = malloc(N_DPU * sizeof(int));
	for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
		nb_points_per_dpu[id_dpu] = N / N_DPU + (N % N_DPU <= id_dpu ? 0 : 1);
	}

	// Send general information to DPUs
	int INFO[8];
	for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
		timing = wtime();
		if (!dpu_load(DPUs[id_dpu], dpu_program)) {
			fprintf(stderr, "Failed to load program!\n");
			dpu_free(DPUs[id_dpu]);
			return -1;
		}
		timing = wtime() - timing;
		load_dpu_time += timing;

		INFO[0] = nb_points_per_dpu[id_dpu];
		INFO[1] = D;
		INFO[2] = K;
		INFO[3] = N_THREAD;
		copy_to_dpu(DPUs[id_dpu], INFO, 0, sizeof(INFO));
	}
	init_dpu_time = wtime() - init_dpu_time;

	// Adresses of objects in the MRAM
	const mram_addr_t centroids_addr = sizeof(INFO);
	const mram_addr_t points_addr    = centroids_addr + D * K * sizeof(int);

	mram_addr_t *result_addr = malloc(N_DPU * sizeof(mram_addr_t));
	for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
		result_addr[id_dpu] = (points_addr + nb_points_per_dpu[id_dpu] * D *
				sizeof(int)) / BLOCK_SIZE * BLOCK_SIZE + BLOCK_SIZE;

		if(result_addr[id_dpu] + (K * (D + 1) * sizeof(int)) > MAX_MRAM_ADDR) {
			fprintf(stderr, "Not enough space: increase the number of DPUs\n");
			exit(OUT_OF_MEMORY);
		}
	}

	// Send points to DPUs
	send_points_time = wtime();
	for (id_dpu = 0, id_point = 0; id_dpu < N_DPU;
			id_point += nb_points_per_dpu[id_dpu], ++id_dpu) {
		copy_to_dpu(DPUs[id_dpu], get_line(&DATA, id_point),
				points_addr, nb_points_per_dpu[id_dpu] * D * sizeof(int));
	}
	send_points_time = wtime() - send_points_time;
	free(nb_points_per_dpu);

#ifdef VERBOSE
	printf("\n|*** Computation ***|\n");
#endif

	computation_time = wtime();

	init_c = wtime();
	// Centroids initialization
	size_t sizeof_centroids        = K * D * sizeof(int);
	size_t sizeof_centroids_update = K * (D + 1) * sizeof(int);
	int *centroids                 = malloc(sizeof_centroids);
	int *centroids_update          = malloc(sizeof_centroids_update);

	init_centroids(centroids, K, N, D, &DATA);
	init_c = wtime() - init_c;

	int one_more_loop  = 1;
	int *DPUs_status   = malloc(N_DPU * sizeof(int)); // 1 Running, 0 finished
	int *centroids_new = malloc(sizeof_centroids_update);

	// While there are cluster switches
	while (one_more_loop > 0) {
		one_more_loop = 0;

		// Initialize centroids_update to 0
		for (id_dim = 0; id_dim < K * (D + 1); id_dim++) {
			centroids_update[id_dim] = 0;
		}

		// Send the centroids to the DPUs
		for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
			copy_to_dpu(DPUs[id_dpu], centroids,
					centroids_addr, sizeof_centroids);
			copy_to_dpu(DPUs[id_dpu], centroids_update,
					result_addr[id_dpu], sizeof_centroids_update);

			// Start DPU program asynchronously
			timing = wtime();

			dpu_boot(DPUs[id_dpu], NO_WAIT);
			durations[id_dpu] = dpu_time(DPUs[id_dpu]);
			timing = wtime() - timing;
			boot_dpu_time += timing;
		}

		// Waiting for the DPUs termination
		int nb_dpus_running = N_DPU;
		for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
			DPUs_status[id_dpu] = 1;
		}

		timing = wtime();

		while (nb_dpus_running > 0) {
			// Checking on DPUs
			for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
				if (DPUs_status[id_dpu] == 1) {
					switch (dpu_get_status(DPUs[id_dpu])) {
						case STATUS_IDLE:
							nb_dpus_running--;
							DPUs_status[id_dpu] = 0;
							int mb;
							dpu_receive(DPUs[id_dpu], 0, 0, &mb, sizeof(int));
							if((time = dpu_time(DPUs[id_dpu])) > durations[id_dpu]) {
								durations[id_dpu] = time - durations[id_dpu];
							} else {
								durations[id_dpu] = ULLONG_MAX - durations[id_dpu] + time;
							}
							//printf("Mailbox of DPU %d: %d\n", id_dpu, mb);
							break;

						case STATUS_ERROR:
							nb_dpus_running--;
							printf("DPU #%d is in error!!!\n", id_dpu);
							dpu_free(DPUs[id_dpu]);
							exit(ERR_DPU);

						default: // Nothing to do.
							break;
					}
				}
			}
		}

		timing = wtime() - timing;
		waiting_time += timing;

		for (id_dpu = 0; id_dpu < N_DPU-1; id_dpu++) {
			printf("%" PRIu64 ",", durations[id_dpu]);
		}
		printf("%" PRIu64 "\n", durations[N_DPU-1]);

		timing2 = wtime();
		// Gather the results
		for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
			copy_from_dpu(DPUs[id_dpu], result_addr[id_dpu],
					centroids_new, sizeof_centroids_update);
			for (id_dim = 0; id_dim < K * (D + 1); id_dim++) {
				centroids_update[id_dim] += centroids_new[id_dim];
			}
		}
#ifdef VERBOSE
		printf("New centroids");
		for (id_dim = 0; id_dim < K * (D + 1); id_dim++) {
			if(id_dim % (D + 1) == 0)
				printf("\n%d,", centroids_update[id_dim]);
			else
				printf("%d,", centroids_update[id_dim]);
		}
		printf("\n");
#endif

		// Divise to end mean computation
		for (id_k = 0; id_k < K; id_k++) {
			if(centroids_update[id_k * (D + 1) + D] != 0) {
				for (id_dim = 0; id_dim < D; id_dim++) {
					centroids_update[id_k * (D + 1) + id_dim] /=
						centroids_update[id_k * (D + 1) + D];
				}
			}
		}

		// Notice a cluster change
		for (id_k = 0; id_k < K; id_k++) {
			for (id_dim = 0; id_dim < D; id_dim++) {
				if(centroids[id_k * D + id_dim] !=
						centroids_update[id_k * (D + 1) + id_dim]) {
					one_more_loop = 1;
					break;
				}
			}
		}

		// Print the durations
		for(id_dpu = 0; id_dpu < N_DPU - 1; id_dpu++) {
			printf("%" PRIu64 ",", durations[id_dpu]);
		}
		printf("%" PRIu64 "\n", durations[N_DPU - 1]);

		// Update the centroids
		for (id_k = 0; id_k < K; id_k++) {
			for (id_dim = 0; id_dim < D; id_dim++) {
				centroids[id_k * D + id_dim] =
					centroids_update[id_k * (D + 1) + id_dim];
			}
		}

		timing2 = wtime() - timing2;
		update_c += timing2;
	}

	computation_time = wtime() - computation_time;
	computation_time = computation_time - waiting_time;

#ifdef VERBOSE
	printf("\n|*** Result ***|\n");
	print_centroids(centroids, D, K);
	printf("\n");
#endif
	store_result(DEST, centroids, D, K);

	for (id_dpu = 0; id_dpu < N_DPU; id_dpu++) {
		dpu_free(DPUs[id_dpu]);
	}

	free(DPUs);
	free(durations);
	free(centroids);
	free(centroids_update);
	free(centroids_new);
	free(DPUs_status);
	free(result_addr);

	total_time = wtime() - total_time;

	printf("Times> ");
	printf("IO:%g, init_dpu:%g, send_points:%g, computation:%g, waiting:%g, "
			"total:%g, get_dpu:%g, load_dpu:%g, boot_dpu:%g, init_c:%g, "
			"update_c:%g\n", IO_time, init_dpu_time, send_points_time,
			computation_time, waiting_time, total_time, get_dpu_time,
			load_dpu_time, boot_dpu_time, init_c, update_c);

	return 0;
}
