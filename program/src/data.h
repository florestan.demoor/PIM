/** @file */ 

#ifndef DATA_H
#define DATA_H

/** Type of the data => temporary, change it to parametric types */
typedef int type_t;
typedef unsigned int uint;

/**  Structure used to store the data: array of size N*M */
typedef struct {
  uint N, M;
  type_t *array;
} data_t;

/** Allocates a data_t structure in size N*M */
data_t init_data(const uint N, const uint M);

/** Deletes a data_t structure */
void free_data(data_t *data);

/** Returns data[i][j] */
type_t get_data(const data_t *data, const uint i, const uint j);

/** Returns the line i of the array */
type_t* get_line(const data_t *data, const uint i);

/** Sets data[i][j] */
void set_data(data_t *data, const uint i, const uint j, const type_t val);

/** Prints a data_t */
void print_data(const data_t *data);
#endif
