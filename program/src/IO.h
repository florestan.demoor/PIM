/** @file */ 

#ifndef PARSER_CSV_H
#define PARSER_CSV_H

#include "data.h"

/** Extract a data_t object from CSV_type file in read mode */
data_t load_data(const char *path);

/** Store a data_t object in a write-mode file */
void store_data(FILE *f, data_t d);

void store_result(const char *dest, int *centroids, int D, int K);

void print_centroids(int* centroids, int D, int K);

void print_point(int* p, int D);

#endif
