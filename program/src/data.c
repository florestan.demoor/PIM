#include <stdio.h>
#include <stdlib.h>

#include "data.h"

data_t init_data(const uint N, const uint M) {
  data_t data;
  data.N = N;
  data.M = M;
  data.array = calloc(N*M, sizeof(type_t));
  return data;
}

void free_data(data_t *data) {
  free(data->array);
}

type_t get_data(const data_t *data, const uint i, const uint j) {
  return *(data->array + i * (data->M) + j);
}

type_t* get_line(const data_t *data, const uint i) {
  return data->array + i * (data->M);
}

void set_data(data_t *data, const uint i, const uint j, const type_t val) {
  *(data->array + i * (data->M) + j) = val;
}

void print_data(const data_t *data) {
  uint i, j;
  printf("Size: %d * %d\n", data->N, data->M);
  for(i = 0; i < data->N; ++i) {
    for(j = 0; j < data->M; ++j) {
      printf("%d ", get_data(data, i, j));
    }
    printf("\n");
  }
}
