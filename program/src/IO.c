#include <stdlib.h>
#include <stdio.h>

#include "IO.h"

#define ERR 1

int find_d (FILE *f) {
	rewind(f);
	int d = 0;
	char c;
	do {
		if (fscanf(f, "%*d%c", &c) < 1) {
			perror("Wrong Format during get_d");
			return -1;
		}
		d++;
	} while (c != '\r' && c != '\n');
	return d;
}

int find_n (FILE *f, int d) {
	rewind(f);
	int n = 0;
	int x;
	char c;
	do {
		if (fscanf(f, "%d", &x) < 1) {
			//perror("Wrong Format during get_n");
			//return -1;
			break;
		}
		n++;
		c = fgetc(f);
	} while (c != EOF);
	return n/d;
}

void find_dat (FILE *f, int* dat) {
	rewind(f);
	int i = 0;
	int x;
	char c;
	do {
		if (fscanf(f, "%d", &x) < 1) {
			//perror("Wrong Format during get_n");
			return;
		}
		dat[i] = x;
		i++;
		c = fgetc(f);
	} while (c != EOF);
}

data_t load_data(const char *path) {
	FILE *f = NULL;
	if((f = fopen(path, "r")) == NULL) {
		perror(path);
		exit(1);
	}
	int d = find_d(f);
	int n = find_n(f, d);
	data_t dat = init_data(n, d);
	find_dat (f, dat.array);
	fclose(f);
	return dat;
}

void store_data(FILE *f, const data_t d) {
	int n = d.N;
	int m = d.M;
	int i;
	char c;
	for (i = 0; i < n * m; i++) {
		c = (i+1)%m ? ',' : '\n';
		fprintf(f, "%d%c", get_data(&d, i/m, i%m), c);
	}
}

void store_result(const char *dest, int *centroids, int D, int K) {
	FILE *fp =  NULL;
	int i, j;
	if((fp = fopen(dest, "w")) == NULL) {
		perror("Error opening the destination file");
		exit(ERR);
	}
	for (i = 0; i < K; i++) {
		for (j = 0; j < D-1; j++) {
			fprintf(fp, "%d,", centroids[i * D + j]);
		}
		fprintf(fp, "%d\n", centroids[i * D + D - 1]);
	}
	fclose(fp);
}

void print_centroids(int* centroids, int D, int K) {
	int i, j;
	printf("Centroids are:\n");
	for (i = 0; i < K; i++) {
		for (j = 0; j < D; j++) {
			printf("%d ", centroids[i * D + j]);
		}
		printf("\n");
	}
}

void print_point(int* p, int D) {
	int i;
	for (i = 0; i < D; i++) {
		printf("%d ", p[i]);
	}
	printf("\n");
}
