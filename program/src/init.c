/** @file */

#include "init.h"
#include <limits.h>


#ifdef ENHANCED_VER
void euclidean_dist(data_t *DATA, int id_k1, int id_k2, const uint nb_dims,
		mpz_t total) {
	mpz_t temp;
	mpz_init(temp);
	uint id_dim;
	for (id_dim = 0; id_dim < nb_dims; id_dim++) {
		mpz_mul_ui(temp, temp, 0);
		mpz_add_ui(temp, temp, get_data(DATA, id_k1, id_dim));
		mpz_sub_ui(temp, temp, get_data(DATA, id_k2, id_dim));
		mpz_mul(total, temp, temp);
	}
	mpz_clear(temp);
}


// chose a new center with probability proportional to distance^2
uint rng_weight(mpz_t *distance, uint *table_points, uint nb_points) {
	mpz_t sum_of_weights;
	mpz_init(sum_of_weights);

	uint i;
	for (i = 0; i < nb_points; i++)
		mpz_add(sum_of_weights, sum_of_weights, distance[table_points[i]]);

	mpz_urandomm(rand_Num, r_state, sum_of_weights);

	for (i = 0; i < nb_points-1; i++) {
		if (mpz_cmp(rand_Num, distance[table_points[i]]) < 0)
			break;
		mpz_sub(rand_Num, rand_Num, distance[table_points[i]]);
	}
	mpz_clear(sum_of_weights);
	return i;
}
#endif


void init_centroids(int *centroids, const uint nb_clusters,
		const uint nb_points, const uint nb_dims, data_t *DATA) {
	uint id_k, id_dim;
#ifndef ENHANCED_VER
	for (id_k = 0; id_k < nb_clusters; id_k++) {
		for (id_dim = 0; id_dim < nb_dims; id_dim++) {
			centroids[id_k * nb_dims + id_dim] = get_data(DATA, id_k, id_dim);
		}
	}
#else // kmeans++
	// init rng
	gmp_randinit_default(r_state);
	gmp_randseed_ui(r_state, 1337);
	mpz_init(rand_Num);

	// random first center
	// id_k = 0;

	uint chosen_center = gmp_urandomm_ui(r_state, nb_points);

	for (id_dim = 0; id_dim < nb_dims; id_dim++)
		centroids[id_dim] = get_data(DATA, chosen_center, id_dim);

	// init stuff
	uint k;
	// correspondence between the number of remaining points and their index
	uint *remaining_points = (uint *) malloc((nb_points - 1) * sizeof(uint));
	for (k = 0; k < nb_points - 1; k++)
		if (k >= chosen_center) remaining_points[k] = k + 1;
		else remaining_points[k] = k;
	// distance to nearest center for each point
	mpz_t *distances = (mpz_t *) malloc(nb_points * sizeof(mpz_t));
	for (k = 0; k < nb_points; k++) {
		mpz_init(distances[k]);
		euclidean_dist(DATA, chosen_center, k, nb_dims, distances[k]);
	}

	// chose the rest of centers
	for (id_k = 1; id_k < nb_clusters; id_k++) {
		// for each data point, the distance^2 to nearest center
		for (k = 0; k < nb_points; k++) {
			mpz_t temp_dist;
			mpz_init(temp_dist);
			euclidean_dist(DATA, chosen_center, k, nb_dims, temp_dist);
			if (mpz_cmp(temp_dist, distances[k]) < 0)
				mpz_add_ui(distances[k], temp_dist, 0);
			mpz_clear(temp_dist);
		}

		// chose a new center with probability proportional to distance^2
		chosen_center = remaining_points[rng_weight(distances, remaining_points,
				nb_points - id_k)];
		for (id_dim = 0; id_dim < nb_dims; id_dim++)
			centroids[id_k * nb_dims + id_dim] = get_data(DATA, chosen_center,
					id_dim);

		// update remaining_points
		for (k = 0; k < nb_points - id_k - 1; k++)
			if (remaining_points[k] >= chosen_center)
				remaining_points[k] = remaining_points[k+1];
	}

	for (k = 0; k < nb_points; k++) {
		mpz_clear(distances[k]);
	}
	free(remaining_points);
	free(distances);

	gmp_randclear(r_state);
	mpz_clear(rand_Num);
#endif
}
