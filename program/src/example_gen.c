#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

/* Size of the hyper-cube containing the points */
#define SIZE 1024

#define PI 3.14159265358979323846264338327950288

#define EPS 0.001

#define STD_DEV 64

/* Return a random float between 0 and 1 */
double rand_float(void) {
	return ((double) rand() / ((double) RAND_MAX + 1));
}

/* Return a random float with a normal gaussian distribution */
double rand_gaussian(void) {
	float u1 = rand_float();
	float u2 = rand_float();
	return sqrt(-2 * log(u1)) * sin(2*PI*u2);
}

/* Compute the norm of a vector in dimension dim */
double norm (int dim, const double *vector) {
	int i;
	double res = 0.;
	for (i = 0; i < dim; i++) {
		res += vector[i] * vector[i];
	}
	return sqrt(res);
}

/* multiply a vector by a scalar in dimension dim */
void mult (int dim, double lambda, double *vector) {
	int i;
	for (i = 0; i < dim; i++) {
		vector[i] *= lambda;
	}
}

/* perform x <- x + y in dimension dim */
void shift (int dim, double *x, const double *y) {
	int i;
	for (i = 0; i < dim; i++) {
		x[i] = x[i] + y[i];
	}
}

/* Generate a random point around zero with standard deviation sigma */
void random_centered_point (int dim, double sigma, double *point) {
  int i;
  double norm_point;
  // Choose a random non-null direction
  do {
    for (i=0; i<dim; i++) {
      point[i] = rand_gaussian();
    }
    norm_point = norm(dim, point);
  } while (norm_point < EPS);
  
  // Choose a distance from the center
  double dist = sigma * rand_gaussian();
  
  // Normalize the point to this new distance
  mult(dim, dist/norm_point, point);
}

/* Convert a double array to a int one */
void intify (int size, const double *in, int *out) {
	int i;
	for (i = 0; i < size; i++) {
		out[i] = (int) in[i];
	}
}

void add_point(FILE *f, int dim, int k, const double *centroids) {
	// Initialize a point around zero
	double *point = (double*)malloc(dim * sizeof(double));
	random_centered_point(dim, STD_DEV, point);

	// Choose centroid
	int which_centroid = (int) (rand_float() * k);

	// Shift the point to the location of the centroid
	shift(dim, point, &centroids[dim*which_centroid]);

	// Convert to int
	int *point_int = (int*)malloc(dim * sizeof(int));
	intify(dim, point, point_int);

	free(point);

	// Output to f
	int i;
	for (i=0; i<dim-1; i++) {
		fprintf(f, "%d,", point_int[i]);
	}
	fprintf(f, "%d\n", point_int[dim-1]);

	free(point_int);
}


int main(int argc, char **argv) {
	if (argc != 6) {
		printf("Usage: ./example_gen [name of the file] [number of points] [dimension] [number of clusters] [seed]\n");
		return -1;
	}
	// name of the output file for the points
	char *name = argv[1];
	// number of points
	int n = atoi(argv[2]);
	// dimension
	int dim = atoi(argv[3]);
	// number of centroids
	int k = atoi(argv[4]);
	// see of the RNG
	int seed = atoi(argv[5]);

	srand(seed);

	int i, j;

	// Initialize centroids
	double *centroids = malloc(dim * k *sizeof(double));
	for (i = 0; i < dim * k; i++) {
		centroids[i] = rand_float() * SIZE;
	}

	// Output points
	FILE *f = fopen(name, "w");
	if (!f) {
		perror("couldn't open file");
		return -1;
	}
	for (i = 0; i < n; i++) {
		add_point(f, dim, k, centroids);
	}
	fclose(f);

	// Convert centroids to int
	int *centroids_int = malloc(dim * k * sizeof(int));
	intify(dim*k, centroids, centroids_int);
	free(centroids);

	// name of the output file for the centroids
	char log_name[1024];
	strcpy(log_name, name);
	strcat(log_name, ".centroids.csv");

	FILE *f_log = fopen(log_name, "w");
	if (!f_log) {
		perror("couldn't open file");
		return -1;
	}

	for (i=0; i<k; i++) {
		for (j=0; j<dim-1; j++) {
			fprintf(f_log, "%d,", centroids_int[i * k + j]);
		}
		fprintf(f_log, "%d\n", centroids_int[i * k + dim -1]);
	}
	fclose(f_log);
	free(centroids_int);
	return 0;
}


