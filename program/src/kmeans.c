#include <mram.h>
#include <alloc.h>
#include <sys.h>
#include <defs.h>
#include <devprivate.h>
#include <mutex.h>
#include <seqread.h>
#include <stdint.h>

#include "tools.c"

#define MUTEX_ID     0 // For thread synchronization
#define BLOCK_SIZE 256 // Size of block to read
#define INFO_SIZE   32 // Size of the information structure
#define MAX_INT (unsigned int) 0xffffffff


/**
 * Program executed by each tasklet :
 * Each tasklet processes N / NB_THREAD
 * - Get general informations
 * - Compute the nearest centroid for each point
 * - Write the result back into the MRAM
 * - Start updating centroids and put results in MRAM (mutex on threads)
 */

/**
 * Memory layout:
 *	- Informations
 *	- Centroids
 *	- Points
 *	- Blank space to get to a multiple of 256
 *	- Results (writing area)
 */

int main(void) {
	mutex_t mutex = mutex_get(MUTEX_ID);
	unsigned int id, id_k, id_dim, id_centroids;

	// Cache for the sequential reader
	void *cache = seqread_alloc();

	unsigned int N_TOTAL,   // Number of points computed by the DPU
				 N,         // Number of points computed by the thread
				 D,         // Dimension
				 K,         // Number of clusters
				 NB_THREAD, // Number of threads
				 offset;    // Offset of the thread
	thread_id_t tid = whoami() - 1;

	// Getting general informations
	int *info;
	seqreader_t sr = seqread_init(cache, 0, (void**) &info);
	N_TOTAL   = info[0];
	D         = info[1];
	K         = info[2];
	NB_THREAD = info[3];
	offset    = tid * (N_TOTAL / NB_THREAD);
	N = tid != (NB_THREAD - 1) ? N_TOTAL / NB_THREAD : N_TOTAL - offset;

	//tell(tid, "0x0001");
	//tell(N_TOTAL, "0x0002");
	//tell(D, "0x0003");
	//tell(K, "0x0004");
	//tell(NB_THREAD, "0x0005");
	//tell(offset, "0x0006");
	//tell(N, "0x0007");
	
	// Address of the different data parts
	const mram_addr_t centroids_addr = INFO_SIZE;
	const mram_addr_t points_addr    = centroids_addr + K * D * sizeof(int);
	const mram_addr_t my_data_addr   = points_addr + offset * D * sizeof(int);
	const mram_addr_t result_addr    = (points_addr + N_TOTAL * D * sizeof(int))
		/ BLOCK_SIZE * BLOCK_SIZE + BLOCK_SIZE;

	// Reading the centroids from the MRAM
	size_t sizeof_centroids = (K * D * sizeof(int));
	int *centroids = mem_alloc(sizeof_centroids);
	size_t sizeof_point = D * sizeof(int);
	int *point;

	id_centroids = 0;
	//TODO: use memset
	sr = seqread_init(cache, centroids_addr, (void**) &point);
	for(id_k = 0; id_k < K; id_k++) {
		for(id_dim = 0; id_dim < D; id_dim++) {
			centroids[id_centroids] = *point;
			id_centroids++;
			point = seqread_get(point, sizeof(int), &sr);
		}
	}

	/*// Print centroids
	for (id_k = 0; id_k < K * D; id_k++) {
			tell(centroids[id_k], "0x0011");
			}*/

	// Buffer containing update informations for the centroids
	size_t sizeof_centroids_update = K * (D + 1) * sizeof(int);
	int* centroids_update = mem_alloc(sizeof_centroids_update);
	// Initialize the structure to 0 TODO: use memset
	for (id = 0; id < K * (D + 1); id++) {
		centroids_update[id] = 0;
	}
	// Calculate the index of centroids to avoid multiplications
	int *index_centroids_update = mem_alloc(K * sizeof(int));
	index_centroids_update[0] = 0;
	for (id_k = 1; id_k < K; id_k++) {
		index_centroids_update[id_k] = index_centroids_update[id_k-1] + D + 1;
	}

	// Main loop
	int *current_point = mem_alloc(sizeof_point);
	sr = seqread_init(cache, my_data_addr, (void**) &point);
	for (id = 0; id < N; id++) {
		unsigned int min = MAX_INT;
		unsigned int cluster_id = -1;
		id_centroids = 0;

		// Get the point from the mram
		for(id_dim = 0; id_dim < D; id_dim++) {
			current_point[id_dim] = *point;
			point = seqread_get(point, sizeof(int), &sr);
		}

		/*// Print the point
		for (id_dim = 0; id_dim < D; id_dim++) {
			tell(current_point[id_dim], "0x0022");
		}*/

		// Computation of the min
		for (id_k = 0; id_k < K; id_k++) {
			unsigned int dist = 0;
			for (id_dim = 0; id_dim < D; id_dim++) {
				int diff = current_point[id_dim] - centroids[id_centroids];
				dist += diff * diff;
				id_centroids++;
			}
			if (dist <= min) {
				min = dist;
				cluster_id = id_k;
			}
		}

		// Update centroids
		id_centroids = index_centroids_update[cluster_id];
		for (id_dim = 0; id_dim < D; id_dim++) {
			centroids_update[id_centroids] += current_point[id_dim];
			id_centroids++;
		}
		centroids_update[id_centroids]++;
	}

	// Update centroids in the MRAM
	size_t buff_size     = BLOCK_SIZE / sizeof(int);
	int* buffer          = mem_alloc_dma(BLOCK_SIZE);
	unsigned int id_buff = 0;
	int addr_buff        = result_addr;
	
	// Print centroids
	/*for (id_k = 0; id_k < K * (D + 1); id_k++) {
	  tell(centroids_update[id_k], "0x0011");
		}*/

	// Update centroids in the MRAM
	mutex_lock(mutex);

	mram_read256(addr_buff, buffer);

	for (id_k = 0; id_k < K * (D + 1); id_k++) {
		if (id_buff >= buff_size) {
			// Write current buffer content
			mram_write256(buffer, addr_buff);
			addr_buff += BLOCK_SIZE;
			// Load next buffer
			mram_read256(addr_buff, buffer);
			id_buff = 0;
		}
		buffer[id_buff] += centroids_update[id_k];
		id_buff++;
	}
	// Write Last buffer
	mram_write256(buffer, addr_buff);

	mutex_unlock(mutex);

	return 0;
}
