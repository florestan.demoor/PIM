#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"

#define IO_ERR 1

conf_t get_conf(const char* path_conf) {
  FILE *fp;
  if((fp = fopen(path_conf, "r")) == NULL) {
    perror(path_conf);
    exit(IO_ERR);
  }
  conf_t conf;
  fscanf(fp, "NB_DPUS=%d\n", &conf.nb_dpus);
  fscanf(fp, "NB_THREADS=%d\n", &conf.nb_threads);
	fscanf(fp, "NB_CLUSTERS=%d\n",&conf.nb_clusters);
  fscanf(fp, "PATH=\"%s\n", conf.path);
	fscanf(fp, "DEST=\"%s", conf.dest);
  if(strlen(conf.path) > 0)
    conf.path[strlen(conf.path) - 1] = '\0';
	if(strlen(conf.dest) > 0)
		conf.dest[strlen(conf.dest) - 1] = '\0';
  fclose(fp);
  return conf;
}

void print_conf(const conf_t *conf) {
  printf("NB_DPUS=%d\n", conf->nb_dpus);
  printf("NB_THREADS=%d\n", conf->nb_threads);
	printf("NB_CLUSTERS=%d\n", conf->nb_clusters);  
	printf("PATH=\"%s\"\n", conf->path);
	printf("DEST=\"%s\"\n", conf->dest);
}
