#include "conf.h"

int main() {
  conf_t conf = get_conf("./config.conf");
  print_conf(&conf);
  return 0;
}
