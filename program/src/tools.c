/** @file */ 


/** \fn int get_next_2power(int n)
 *  \param n  Integer
 *  \return Power of 2 >= 8 and directly >= n, or -1 if n >= 256
 */
 
int get_next_2power(int n) {
	if (n <= 8) { return 8; }
	if (n <= 16) { return 16; }
	if (n <= 32) { return 32; }
	if (n <= 64) { return 64; }
	if (n <= 128) { return 128; }
	if (n <= 256) { return 256; }
	return -1; /* Not supported, D must be <= 63 */
}
