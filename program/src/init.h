/** @file */

#ifndef INIT_H
#define INIT_H

#include <stdlib.h>
#include "data.h"

// #define ENHANCED_VER
// #include <gmp.h>

#ifdef ENHANCED_VER
mpz_t rand_Num;
gmp_randstate_t r_state;
#endif

/** Initialize centroids */
void init_centroids(int *centroids, const uint nb_clusters,
		const uint nb_points, const uint nb_dims, data_t *DATA);
#endif
