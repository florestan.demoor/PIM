/** @file */ 

#define PATH_SIZE 128

typedef struct {
  unsigned int nb_dpus;   
  unsigned int nb_threads;
  unsigned int nb_clusters;
	char path[PATH_SIZE];
	char dest[PATH_SIZE];
} conf_t;

/* Returns the conf_t struct from the file located at path_conf */
conf_t get_conf(const char* path_conf);

/* Prints the content of the conf_t struct */
void print_conf(const conf_t *conf);
