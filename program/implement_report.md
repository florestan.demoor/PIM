# kmeans++
- euclidean distances are bounded by ULONG_MAX
- weighted rng
  + computing the sum of distances and picking a random number between 0 and this sum
  + if an overflow happens it will just not pick between all points
