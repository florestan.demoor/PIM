#! /usr/bin/python3

from sklearn.cluster import KMeans
import numpy as np

nb_clusters = 3
dimensions = 8
nb_init_points = 3


class int_special(int):
    def __new__(cls, value):
        return int.__new__(cls, value)

    def __div__(self, other):
        if isinstance(other, self.__class__):
            return self.value // other
        else:
            return int.__div__(self.value, other)


if __name__ == '__main__':
    DATA = [[14, 63, 64, 96, 57, 74, 66, 28],
            [51, 43, 91, 72, 17, 15, 33, 89],
            [10, 40, 88, 78, 83, 53, 10, 55],
            [74, 93, 23, 76, 91, 55, 49, 54],
            [47, 15, 61, 87, 80, 53, 72, 47],
            [21, 39, 63, 53, 60, 70, 87, 49],
            [44, 37, 15, 61, 52, 35, 93, 95],
            [54, 46, 73, 41, 23, 56, 39, 46],
            [23, 26, 44, 59, 13, 43, 22, 38],
            [28, 82, 77, 30, 23, 80, 54, 69],
            [88, 38, 62, 44, 80, 47, 60, 67],
            [30, 50, 58, 23, 86, 18, 76, 81],
            [80, 43, 14, 14, 77, 74, 26, 39],
            [27, 21, 78, 70, 95, 95, 49, 47],
            [80, 89, 49, 56, 83, 77, 85, 12],
            [45, 26, 15, 30, 32, 61, 78, 19]]

    DATA = np.array([[int_special(y) for y in x] for x in DATA])

    # I don't understand why nb_centroids != nb_clusters
    kmeans = KMeans(n_clusters=nb_clusters, init=DATA[:nb_clusters],
                    verbose=1).fit(DATA)
    print(kmeans.cluster_centers_)
    
    
def d(l1, l2):
  s = 0
  for i in range(len(l1)):
    s += (l1[i] - l2[i]) ** 2
  return n
    
    
def kmeans(DATA, k):
  
  C = DATA[:k]
  N = len(C)
  assigned = [-1 for i in range(N)]
  
  for i in range(N):
    
    mini = d(DATA[i], C[0])
    minID = 0
    
    for j in range(1, k):
      x = d(DATA[i], C[j])
      
      if (x < mini):
        mini = x
        minID = j
