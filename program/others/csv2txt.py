import sys

if (len(sys.argv) != 2):
  print("Usage: python3 csv2txt.py <file.csv>")
  assert(False)

path = sys.argv[1]
f_read = open(path, 'r')
f_write = open(path.replace('.csv', '.txt'), 'w')

index = 1

for line in f_read:
  f_write.write(str(index) + ' ')
  f_write.write(line.replace(',', ' '))
  index += 1
  
f_write.close()
f_read.close()
