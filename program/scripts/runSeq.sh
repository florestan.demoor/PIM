#!/bin/bash

SEQ_PATH="others/parallel-kmeans"
SEQ_EXE="./seq_main"
DIR="../program"
CONVERTER="others/csv2txt.py"

CENTROIDS="initial_centroids.temp"

RESULT_MEM=".membership"
RESULT_CEN=".cluster_centres"
RESULT="../../expected_centroids.csv"

cd ..
if [ "$#" -ne 1 ]; then
  FILE="datasets/small_test.csv"
else
  FILE=$1
fi
python3 $CONVERTER $FILE
FILE="${FILE%.*}.txt"
mv $FILE $SEQ_PATH
FILE=${FILE#"datasets/"}
K="3"

cd $SEQ_PATH
head -n $K $FILE > $CENTROIDS 

make seq
./$SEQ_EXE -i $FILE -n $K -t 0 -c $CENTROIDS -o 
mv $FILE$RESULT_CEN $RESULT

cat $RESULT
python3 ../../scripts/minfunc.py $RESULT $FILE txt

make clean
rm $FILE
rm $CENTROIDS
