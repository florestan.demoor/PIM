#!/bin/sh

### Check arguments
if [ "$#" -gt 7 ]; then
    echo "Illegal number of parameters"
fi

###
echo "Generating kmeans config file"
echo "NB_DPUS=$1" > config.conf
echo "NB_THREADS=$2" >> config.conf
echo "NB_CLUSTERS=$3" >> config.conf
echo "PATH=\"$4\"" >> config.conf
echo "DEST=\"$5\"" >> config.conf

###
echo "Generating config for DPUs"
> dpuConfig.sh
for i in $(seq 1 $2); do
	echo "tasklet add $i small main 1" >> dpuConfig.sh
done
echo "mutex 1" >> dpuConfig.sh
echo "save config.json" >> dpuConfig.sh
echo "quit" >> dpuConfig.sh
dpukconfig -s dpuConfig.sh

echo "Compile DPUs program"
if [ "$7" = "1" ]; then
	if [ "$6" = "CAS" ]; then
		make dpu_CAS
	else
		make dpu
	fi
fi

###
rm dpuConfig.sh
rm config.json
