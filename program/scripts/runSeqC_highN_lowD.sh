#!/bin/sh

cd ..

## Check presence of executables

if [ ! -f example_gen ]; then
  make example_gen
fi

## KMEANS CONFIG
N=1000000
D=9
K=5
if [ "$#" -ne 1 ]; then
  DATASET="datasets/random.csv"
else
  DATASET=$1
fi
OUTPUT="result_centroids.txt"
SEED=512141568456

NB_RUNS=1

### OTHER
CONVERTER="others/csv2txt.py"
SEQ_PATH="others/parallel-kmeans"
SEQ_EXE="seq_main"

for j in $(seq 1 $NB_RUNS); do

  OUTPUT_DIR="output/seqC_highN_lowD/$(date +%Y-%m-%d_%Hh%Mmin%Ss)"
  mkdir --parents $OUTPUT_DIR

  ## Generate dataset
  echo "Generating dataset $j / $NB_RUNS"
  ./example_gen $DATASET $N $D $K $(($SEED + $j))
  if [ $? -ne 0 ]; then
    echo "Error: Aborting script"
    exit
  fi

  python3 $CONVERTER $DATASET
  FILE="${DATASET%.*}.txt"

  # Compile
  echo "Compiling"
  BASEDIR=$(pwd)
  cd $SEQ_PATH
  make seq
  
  ## Run
  echo "Running program"
  ./$SEQ_EXE -i $BASEDIR/$FILE -n $K -t 0 -o > $BASEDIR/$OUTPUT_DIR/info.log
  
  mv $BASEDIR/$FILE.cluster_centres $BASEDIR/$OUTPUT_DIR/$OUTPUT
  
  # Keep parameters and clean
  echo "Done"
 
  CURRENT_DIR=$(pwd) 
  cd $BASEDIR/$OUTPUT_DIR
  echo "N=$N" > config.log
  echo "D=$D" >> config.log
  echo "K=$K" >> config.log
  echo "DATASET_SEED=$(($SEED + $j))" >> config.log
  
  # Compute quality metric
  cd $BASEDIR
  python3 scripts/minfunc.py ${OUTPUT_DIR}/${OUTPUT} $FILE "txt" > ${OUTPUT_DIR}/min_func.log

  cd $SEQ_PATH
  make clean
  
  cd $BASEDIR
  rm $FILE
  rm $DATASET
  rm $DATASET.centroids.csv
  
done
