#!/bin/sh

## KMEANS CONFIG
N=10736343
D=100
K=8
if [ "$#" -ne 1 ]; then
  DATASET="datasets/random.csv"
else
  DATASET=$1
fi
OUTPUT="result_centroids.csv"
SEED=512141568458

### DPU CONFIG
NB_DPUS=64
NB_THREADS=10

## Generate dataset
cd ..
./example_gen $DATASET $N $D $K $SEED
if [ $? -ne 0 ]; then
  echo "Error: dataset generation failed"
  exit 1
fi

### Generate config file
./scripts/genConfig.sh ${NB_DPUS} ${NB_THREADS} ${K} ${DATASET} ${OUTPUT} "FUNC" "1"
if [ $? -ne 0 ]; then
  echo "Error: tasklet compilation failed"
  exit 1
fi

## Run
echo "Running UPMEM program"
./main
if [ $? -ne 0 ]; then
  echo "Error: while running UPMEM program"
  exit 1
fi

## Output result
echo "Output min function result"
python3 scripts/minfunc.py result_centroids.csv datasets/random.csv csv


echo "Running sequential C program"
cd scripts
./runSeq.sh ${DATASET}


# Clean
rm config.conf
