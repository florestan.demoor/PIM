#!/bin/sh

## KMEANS CONFIG
N=50000
D=33
K=5
if [ "$#" -ne 1 ]; then
  DATASET="datasets/random.csv"
else
  DATASET=$1
fi
OUTPUT="result_centroids.csv"
SEED=512141568456

### DPU CONFIG
NB_DPUS=32
NB_THREADS=10

## Generate dataset
cd ..
./example_gen $DATASET $N $D $K $SEED
if [ $? -ne 0 ]; then
  echo "Error: dataset generation failed"
  exit 1
fi

### Generate config file
./scripts/genConfig.sh ${NB_DPUS} ${NB_THREADS} ${K} ${DATASET} ${OUTPUT} "FUNC" "1"
if [ $? -ne 0 ]; then
  echo "Error: tasklet compilation failed"
  exit 1
fi

## Run
echo "Running program"
./main
if [ $? -ne 0 ]; then
  echo "Error: while running UPMEM program"
  exit 1
fi

# Output the results and clean
echo "Output results"
cat $OUTPUT
# rm config.conf
