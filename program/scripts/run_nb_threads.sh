#!/bin/sh

cd ..
make mrproper

## Check presence of executables
if [ ! -f main ]; then
  #make main_CAS # Cycle-Accurate simulator
  make main # Functionnal simulator
fi

if [ ! -f example_gen ]; then
  make example_gen
fi

## KMEANS CONFIG
N=5000
D=3
K=3
if [ "$#" -ne 1 ]; then
  DATASET="datasets/random.csv"
else
  DATASET=$1
fi
OUTPUT="result_centroids.csv"
SEED=512245668564

### DPU CONFIG
NB_DPUS=16

## Generate dataset
echo "Generating dataset"
./example_gen $DATASET $N $D $K $SEED
if [ $? -ne 0 ]; then
  echo "Error: Aborting script" 
  exit
fi

for NB_THREADS in $(seq 1 23); do
  
  OUTPUT_DIR="output/nb_threads/DATASET19/NB_THREADS_${NB_THREADS}"
  mkdir --parents $OUTPUT_DIR
  
  ### Generate config file
  ./scripts/genConfig.sh ${NB_DPUS} ${NB_THREADS} ${K} ${DATASET} ${OUTPUT_DIR}/${OUTPUT} "FS" "1"

  ## Run
  echo "Running program with ${NB_THREADS} threads"
  ./main > ${OUTPUT_DIR}/time.csv
  if [ $? -ne 0 ]; then
    echo "Error: Aborting script"
    exit
  fi
  
  # Keep parameters and clean
  echo "Done"
 
  CURRENT_DIR=$(pwd) 
  cd $OUTPUT_DIR
  echo "N=$N" > config.log
  echo "D=$D" >> config.log
  echo "K=$K" >> config.log
  echo "DATASET_SEED=$SEED" >> config.log
  echo "NB_DPUS=$NB_DPUS" >> config.log
  echo "NB_THREADS=$NB_THREADS" >> config.log
  cd $CURRENT_DIR
  
  # Compute quality metric
  python3 scripts/minfunc.py ${OUTPUT_DIR}/${OUTPUT} $DATASET "csv" > ${OUTPUT_DIR}/min_func.log

  rm config.conf
done

rm $DATASET
rm $DATASET.centroids.csv 
