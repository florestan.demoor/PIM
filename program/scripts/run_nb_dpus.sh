#!/bin/sh

cd ..
make mrproper

## Check presence of executables
if [ ! -f main ]; then
  #make main_CAS # Cycle-Accurate simulator
  make main # Functionnal simulator
fi

if [ ! -f example_gen ]; then
  make example_gen
fi

## KMEANS CONFIG
N=1000000
D=10
K=5
if [ "$#" -ne 1 ]; then
  DATASET="datasets/random.csv"
else
  DATASET=$1
fi
OUTPUT="result_centroids.csv"
SEED=5122456683464

### DPU CONFIG
NB_THREADS=10

## Generate dataset
echo "Generating dataset"
./example_gen $DATASET $N $D $K $SEED
if [ $? -ne 0 ]; then
  echo "Error: Aborting script" 
  exit
fi

for NB_DPUS in $(seq 1 32); do
  
  OUTPUT_DIR="output/nb_dpus/DATASET1/NB_DPUS_${NB_DPUS}"
  mkdir --parents $OUTPUT_DIR
  
  ### Generate config file
  ./scripts/genConfig.sh ${NB_DPUS} ${NB_THREADS} ${K} ${DATASET} ${OUTPUT_DIR}/${OUTPUT} "FS" "1"

  ## Run
  echo "Running program with ${NB_DPUS} dpus"
  ./main > ${OUTPUT_DIR}/time.csv
  if [ $? -ne 0 ]; then
    echo "Error: Aborting script"
    exit
  fi
  
  # Keep parameters and clean
  echo "Done"
 
  CURRENT_DIR=$(pwd) 
  cd $OUTPUT_DIR
  echo "N=$N" > config.log
  echo "D=$D" >> config.log
  echo "K=$K" >> config.log
  echo "DATASET_SEED=$SEED" >> config.log
  echo "NB_DPUS=$NB_DPUS" >> config.log
  echo "NB_THREADS=$NB_THREADS" >> config.log
  cd $CURRENT_DIR
  
  # Compute quality metric
  python3 scripts/minfunc.py ${OUTPUT_DIR}/${OUTPUT} $DATASET "csv" > ${OUTPUT_DIR}/min_func.log

  rm config.conf

  rm /tmp/hsim_dpu_profiling_*
done

rm $DATASET
rm $DATASET.centroids.csv 
