#!/bin/sh

cd ..
make mrproper

## Check presence of executables
if [ ! -f main ]; then
  make main_CAS # Cycle-Accurate simulator
  #make main # Functionnal simulator
fi

if [ ! -f example_gen ]; then
  make example_gen
fi

## KMEANS CONFIG
N=50000
D=33
K=5
if [ "$#" -ne 1 ]; then
  DATASET="datasets/random.csv"
else
  DATASET=$1
fi
OUTPUT="result_centroids.csv"
SEED=512245668456

### DPU CONFIG
NB_DPUS=32
NB_THREADS=10

NB_RUNS=10

for j in $(seq 1 $NB_RUNS); do
  
  OUTPUT_DIR="output/highN_highD/$(date +%Y-%m-%d_%Hh%Mmin%Ss)"
  mkdir --parents $OUTPUT_DIR

  ## Generate dataset
  echo "Generating dataset $j / $NB_RUNS"
  ./example_gen $DATASET $N $D $K $(($SEED + $j))
  if [ $? -ne 0 ]; then
    echo "Error: Aborting script"
    exit
  fi
  
  ### Generate config file
  if [ "$j" -eq "1" ]; then
	./scripts/genConfig.sh ${NB_DPUS} ${NB_THREADS} ${K} ${DATASET} ${OUTPUT_DIR}/${OUTPUT} "CAS" "1"
  else
	./scripts/genConfig.sh ${NB_DPUS} ${NB_THREADS} ${K} ${DATASET} ${OUTPUT_DIR}/${OUTPUT} "CAS" "0"
  fi

  ## Run
  echo "Running program"
  ./main > ${OUTPUT_DIR}/time.csv
  if [ $? -ne 0 ]; then
    echo "Error: Aborting script"
    exit
  fi
  
  # Keep parameters and clean
  echo "Done"
 
  CURRENT_DIR=$(pwd) 
  cd $OUTPUT_DIR
  echo "N=$N" > config.log
  echo "D=$D" >> config.log
  echo "K=$K" >> config.log
  echo "DATASET_SEED=$(($SEED + $j))" >> config.log
  echo "NB_DPUS=$NB_DPUS" >> config.log
  echo "NB_THREADS=$NB_THREADS" >> config.log
  cd $CURRENT_DIR
  
  # Compute quality metric
  python3 scripts/minfunc.py ${OUTPUT_DIR}/${OUTPUT} $DATASET "csv" > ${OUTPUT_DIR}/min_func.log

  rm $DATASET
  rm $DATASET.centroids.csv
  rm config.conf
  
done
