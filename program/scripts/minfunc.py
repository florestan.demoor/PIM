import sys
from math import sqrt

sep = [',', ' ']


def get_centroids(name, start_id):
  f = open(name, 'r')
  centroids = []
  for line in f:
    point = line.rstrip('\n').split(sep[start_id])
    point = point[start_id:len(point)-start_id]
    point = [int(float(e)) for e in point]
    centroids.append(point)
  f.close()
  return centroids
  

def dist(x, y):
  return sqrt(sum([(a - b)**2 for a, b in zip(x, y)]))

  
def get_closest_distance(point, centroids):
  min_dist = dist(point, centroids[0])
  for i in range(1, len(centroids)):
    tmp_dist = dist(point, centroids[i])
    if tmp_dist < min_dist:
      min_dist = tmp_dist
  return min_dist
  

def get_min_function(name, centroids, start_id):
  f = open(name, 'r')
  min_func = 0
  for line in f:
    point = line.rstrip('\n').split(sep[start_id])
    point = point[start_id:len(point)-start_id]
    point = [int(float(e)) for e in point]
    min_func += get_closest_distance(point, centroids)
  f.close()
  return min_func
  
  
def main():
  
  if len(sys.argv) != 4:
    print("Usage: python3 minfunc.py <centroids_file> <data_file> <txt | csv>")
    exit(1)
    
  PATH_C = sys.argv[1]
  PATH_DATA = sys.argv[2]
  TYPE = sys.argv[3]
  if TYPE not in ['csv', 'txt']:
    print("Invalid last argument: csv | txt")
    exit(1)
  start_id = 0
  if TYPE == 'txt':
    start_id = 1
  centroids = get_centroids(PATH_C, start_id)
  result = get_min_function(PATH_DATA, centroids, start_id)
  print("Function value is", result)
  
  
main()
