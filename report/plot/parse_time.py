def parse_time(path):
    f = open(path, 'r')

    nb_cycles = 0
    host_info = []

    for line in f:
        data = line.rstrip('\n').split(',')
        if data[0][0] != 'T':
            # DPU cycles
            data2 = [int(e) for e in data]
            nb_cycles += max(data2)
        else:
            host_info = [float(e.split(':')[1]) for e in data]

    f.close()

    return nb_cycles, host_info
