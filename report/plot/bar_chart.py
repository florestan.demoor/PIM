import numpy as np
import matplotlib.pyplot as plt

FILE = 'real_quality.dat'
LABELS = { 0: 'DPU based', 1: 'C Sequential', 2: 'C MPI', 3: 'Hadoop', 4: 'CUDA' }
WIDTH = 0.5
COLORS = ['#FF8C00', '#008000', '#B22222', '#1E90FF', '#C71585', '#FF4500', '#1133AA', '#765496', '#973639', '#083861']
DATASETS = ['Real 1', 'Real 2']

DATA = np.loadtxt(FILE, dtype = float)

(N, M) = np.shape(DATA)

fig, ax = plt.subplots()
ind = np.arange(M - 1) * N
Ymax = 0


def DPUnormalize(DATA):
  DPU = np.array([])
  for X in DATA:
    if X[0] == 0:
      DPU = np.copy(X[1:])
      break
  for X in DATA:
    X[1:] = np.divide(X[1:], DPU)


DPUnormalize(DATA)
for i in DATA[:,0]:
  j = int(i)
  X = DATA[j, 1:]  
  Ymax = max(Ymax, max(np.abs(X)))
  ax.bar(ind + WIDTH * j, X, WIDTH, color=COLORS[j], label=LABELS[j])

ax.axis([WIDTH - 1,(M - 1) * N, 0, 1.1 * Ymax])
ax.set_ylabel('Within-cluster Sum of Squares')
ax.set_xticks(ind + WIDTH * N / 2)
ax.set_xticklabels(DATASETS)
ax.grid()
plt.legend(loc=1)

plt.show()
#plt.savefig(FILE.replace('dat', 'svg'))
