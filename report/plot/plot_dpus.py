import numpy as np
from parse_time import parse_time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

BASEDIR = '../../program/output/nb_dpus/'
FILENAME = 'time.csv'

ID_REDUCE = 10
FREQ = 750e6

DATASETS = [1]

A = 1
B = 32

COLORS = ['#FF8C00', '#008000', '#B22222', '#1E90FF', '#C71585', '#FF4500',
          '#1133AA', '#765496', '#973639', '#083861']
MARKERS = ['o', 's', '^', 'd', 'v', '*', 'h', '<', '>', '8']

fig, ax = plt.subplots()

X = np.array([i for i in range(A, B+1)])

color = 0
marker = 0

for i in DATASETS:
    ax.clear()

    DIR = 'DATASET' + str(i)

    Y = np.zeros(B+1-A, dtype=float)
    Y2 = np.zeros(B+1-A, dtype=float)

    for j in range(A, B+1):
        path = BASEDIR + DIR + '/NB_DPUS_' + str(j) + '/' + FILENAME
        nb_cycles, host_info = parse_time(path)

        dpu_time = nb_cycles / FREQ
        Y[j - A] = dpu_time  # + host_info[ID_REDUCE]
        Y2[j - A] = Y[0] / (j - A + 1)

    # Y = Y / Y[0]

    ax.plot(X, Y2, color=COLORS[color + 1], marker=MARKERS[marker + 1],
            label='Ideal')
    ax.plot(X, Y, color=COLORS[color], marker=MARKERS[marker], label=DIR)

    color = (color + 2) % len(COLORS)
    marker = (marker + 2) % len(MARKERS)

    # ax.axis([A,B, 0, 1.1*max(Y)])
    #  ax.ticklabel_format(style='plain')
    ax.set_ylabel('Runtime (seconds)')
    ax.set_xlabel('Number of DPUs')
    ax.grid()
    #  plt.legend(loc=2)

    # plt.show()
    #  plt.savefig('dpus_' + DIR + '.svg')
    plt.savefig('dpus_' + DIR + '.pdf', format='pdf', transparent=True)
