import numpy as np
from parse_time import parse_time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

BASEDIR = '../../program/output/nb_threads/'
FILENAME = 'time.csv'

ID_REDUCE = 10
FREQ = 750e6

DATASETS = [10, 11, 12, 13, 14]

A = 1
B = 23

COLORS = ['#FF8C00', '#008000', '#B22222', '#1E90FF', '#C71585', '#FF4500',
          '#1133AA', '#765496', '#973639', '#083861']
MARKERS = ['o', 's', '^', 'd', 'v', '*', 'h', '<', '>', '8']

fig, ax = plt.subplots()

X = np.array([i for i in range(A, B+1)])

color = 0
marker = 0

for i in DATASETS[:1]:
    DIR = 'DATASET' + str(i)

    Y = np.zeros(B+1-A, dtype=float)

    for j in range(A, B+1):
        path = BASEDIR + DIR + '/NB_THREADS_' + str(j) + '/' + FILENAME
        nb_cycles, host_info = parse_time(path)

        dpu_time = nb_cycles / FREQ
        Y[j - A] = dpu_time  # + host_info[ID_REDUCE]

    # Y = Y / Y[0]

    ax.plot(X, Y, color=COLORS[color], marker=MARKERS[marker], label=DIR)

    color = (color + 1) % len(COLORS)
    marker = (marker + 1) % len(MARKERS)

    #  ax.ticklabel_format(useOffset=False)
    ax.ticklabel_format(style='plain')
    # ax.axis([A,B, 0, 1.1*max(Y)])
    ax.set_ylabel('Runtime')
    ax.set_xlabel('Number of threads')
    ax.get_yaxis().set_ticks([])
    ax.grid()
    #  plt.legend(loc=2)

    # plt.show()
for i in DATASETS[1:3]:
    ax2 = ax.twinx()
    DIR = 'DATASET' + str(i)

    Y = np.zeros(B+1-A, dtype=float)

    for j in range(A, B+1):
        path = BASEDIR + DIR + '/NB_THREADS_' + str(j) + '/' + FILENAME
        nb_cycles, host_info = parse_time(path)

        dpu_time = nb_cycles / FREQ
        Y[j - A] = dpu_time  # + host_info[ID_REDUCE]

    # Y = Y / Y[0]

    ax2.plot(X, Y, color=COLORS[color], marker=MARKERS[marker], label=DIR)

    color = (color + 1) % len(COLORS)
    marker = (marker + 1) % len(MARKERS)
    ax2.get_yaxis().set_ticks([])

plt.savefig('threads_all.pdf', format='pdf', transparent=True)
