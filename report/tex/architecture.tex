\begin{figure}
    \centering
    \includegraphics[width=0.9\columnwidth]{upmem_arch}
    \caption{%
        \textbf{Illustration of \upmem{} architecture}
        % DPU\@: DRAM processing unit,
        % WRAM\@: execution memory of the CPU,
        % MRAM\@: main memory
    }
\label{fig:upmem}
\end{figure}

%Upmem presentation
UPMEM technology relies on the concept of processing in memory (PIM).
Co-processing units called DPUs (standing for DRAM Processing Unit), are integrated into the main memory.
They can execute operations directly into the DIMM, minimizing the data movement.
This concept permits massive parallelization through the DPUs while the bandwidth is increased and the latency is reduced, compared to a standard parallel architecture.
This makes this technology particularly efficient for data-intensive operations.

\subsection{Architecture}
%DPU tasks
DPUs are programmable co-processor optimized for data computing, designed to run small programs or routines on behalf of the CPU\@.
They can be run simultaneously and are independent in term of code and data.
Each of them is attached to a distinct bank of the memory with independent access so the bandwidth is improved since no bus is shared.

%Thread mechanism
The DPUs are based on a RISC architecture with 24 32-bit registers per thread.
The software implementation of an execution thread is called a tasklet and is based on hardware threads.

The DPUs have no data and instruction cache but two fast memories: the instruction RAM called WRAM and a scratchpad memory called MRAM, shared by all the tasklets. The global architecture is shown in \figurename~\ref{fig:upmem}.
The MRAM must be explicitly managed by the programmer who can reserve areas for shared memories or for exclusive tasklet usage (heap allocation).

%Limitations and strength
The architecture is designed to sit in the DRAM technology, which introduces several constraints.
The power of the DPUs is limited compared to a standard processor: the frequency is around 750 MHz and operations they can perform are limited.
For example they cannot perform floating point operations and the multiplication is expansive with an overhead of 30 cycles.

The strength of the technology lies in the data access. The DPUs support multi-threading up to 24 threads, in a way that the context is switched at every clock cycle. A thread executing a direct memory access is suspended until the transfer is complete, insuring minimum latency.

%Parallelization power
One DPU is attached to every 64 MBytes of memory.
Hence, a 16 GBytes DIMM embeds 256 independent DPUs, and several of them can be added to a standard CPU\@.
This makes a total of 4096 cores together that can support up to 24 threads, with 256 GBytes of memory.
However, this massively parallel environment require an adaptation of the software.


\subsection{Programming applications on \upmem{}}
On the programming level, two programs must be specified.

The host processor acts as a coordinator: it allocates the DPUs, loads the program into the DPUs, prepares the data, boots the DPUs and gathers the results.

The data-intensive part of the code is offloaded to the DPUs as tasklets.
Several tasklets can run simultaneously on a DPU and synchronization primitives, as well as share memory mechanisms,  are available to orchestrate the execution.

It can be seen as a distributed system at the server level.
Note that the tasklet execution can be run asynchronously with the host program, allowing host tasks to be overlapped with DPUs tasks.

\subsection{Communication between CPU and DPUs}
The CPU has two mechanisms to communicate with the DPUs: through the MRAM or through a mailbox system.

Communication through the MRAM is similar to a standard communication between the CPU and the memory.
The CPU stores some data into the MRAM, the DPU computes the data, store the result into the MRAM and the CPU loads the result.
This mechanism is rather slows but allows the communication of large amounts of data.

The mailbox mechanism is faster but the amount of shared data does not exceed a few bytes, which makes it ideal for synchronization.
The mailbox is located in the WRAM and can be accessed quickly by the DPU\@.
Both the CPU and the DPU can perform post or receive operations on the mailbox and exchange data.

The DPUs cannot communicate directly but only through the host processor, which makes this operation vulnerable to bandwidth issues.

% Conclusion
To sum up, \upmem\ technology offers a massively parallel environment with increased bandwidth and minimum latency for data-intensive applications.
The application has to be adjusted to consider the distributed architecture format.
The main program supervises the data transfers and the tasklets execution while the DPUs handle the data-intensive part of the code.
