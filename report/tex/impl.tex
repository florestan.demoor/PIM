In this section, we explain how we made a distributed $k$-means.%
~\ref{dist_k-means} presents our algorithm of $k$-means using \upmem{} architecture.
We then give details about our implementation in~\ref{impr_dist_k-means}.

\subsection{Highly Parallelized $k$-means Algorithm}
\label{dist_k-means}

The main idea is to distribute the data across the DPU memories in order to offload the computation of the distances.
The host processor communicates the centroids to the DPUs, gathers and merges the results.
The main advantage is that the distances computations are delegated to the DPUs to be run in parallel near the data.

The following steps are performed:
\begin{enumerate}
  \item The points are uniformly distributed between the DPUs.
  \item The host processor chooses the initial centroids.
  \item The host processor communicates the centroids to the DPUs.
  \item The DPUs assign each point to the nearest centroid.
  \item The DPUs start computing the new centroids with their partial information.
  \item The host processor merges the partial results and computes the new centroids.
  \item Repeat from 3 until the convergence criteria is met.
  \item Return the clusters.
\end{enumerate}


Those steps are illustrated in \figurename~\ref{fig:algo}.

\begin{figure}
  \centering
  \includegraphics[width=0.9\columnwidth]{algo}
  \caption{%
    \textbf{$k$-means on \upmem{}.}
    The points are uniformly distributed across the DPUs.
    Computations consist in calculating the nearest cluster for each point.
  }
\label{fig:algo}
\end{figure}


\subsection{Implementation Details}
\label{impr_dist_k-means}
We expose now the details of the two aspects of the implementation: the host program, and the DPUs tasklet.

\subsubsection{Host program}
First, the dataset is processed to get the data and its characteristics (number of points and dimensions).
Then we request the DPUs, and we initialize them by sending the tasklet program and general information (the value of $k$, or the number of threads requested by the user, \dots).
Next, the points are dispatched uniformly over the DPUs.
The host enters the \texttt{while} loop and starts by sending the current centroids to all DPUs.
It boots the DPUs and wait for them to have all completed their task.
Then, the host retrieves partial centroids computed by the DPUs and performs a reduce task to get the new centroids for the next iteration.
It also checks if the convergence criteria is met, by comparing the old and the new centroids.
If this is not the case, this loop is repeated until convergence.
Otherwise, the hosts writes the results in a logging file and terminates.

\subsubsection{DPU tasklet}
This program is run by every thread of each DPU\@.
% The MRAM is organized as shown in \figurename~\ref{img:mramv2}.
The MRAM stores the followings:
\begin{itemize}
  \item global variables (e.g.\ the number of points),
  \item centroids,
  \item points, and
  \item new centroids.
\end{itemize}
First the thread retrieves the general informations, its ID, and the current centroids, and stores it into the WRAM\@.
Then it loops over every point it is in charge of.
It transfers the point from the MRAM to the WRAM\@.
It performs some computation to determine the closest centroid and does
a partial centroid update, by adding the contribution of the current point to the right centroid.
Once this step is done for every point, the thread adds up its partial centroids computation to the global one in MRAM\@.
We use a critical section as this memory access is shared between all threads.

% \begin{figure}
%   \centering
%   \includegraphics[width=0.8\columnwidth]{mram_organization}
%   \caption{Organization of the DPUs memory}
% \label{img:mramv2}
% \end{figure}


\subsubsection{Limits}
This implementation could still be improved in many ways.
A part of the data used in the tasklet is identical between threads.
Therefore, shared memory could be added to store those data and avoid redundant computation.

Also, since the multiplication is rather expensive in this architecture,
it could be interesting to use another distance without any multiplication during the first steps to approach the result, and only use euclidean distance for the final convergence.
