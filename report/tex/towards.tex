\subsection{KMeans Implementation}

This section presents the strategy elaborated to fully exploit the \upmem{} architecture.
The main idea is to distribute the data across the DPU memories in order to offload the computation of the distances.
The host processor communicates the centroids to the DPUs, gathers and merges the results.
The main advantage is that the distances calculus is delegated to the DPUs to be run in parallel near the data.

The following steps are performed:
\begin{enumerate}
    \item The points are uniformly distributed between the DPUs
	\item The host processor choose the initial centroids
	\item The host processor communicates the centroids to the DPUs
	\item The DPUs assign each point to the nearest centroid
%	\item The DPUs update the centroid of each cluster and send them back to the host processor \FDM{I'm not sure about that} \BT{It's somewhat correct but not clear enough, explaining it in toward
	\item The host processor computes the new centroids
	\item Repeat from 3 until the convergence criteria is met
	\item Return the clusters
\end{enumerate}


\figurename \ref{fig:algo} illustrates those steps.

\begin{figure}
  \centering
      \includegraphics[width=0.9\columnwidth]{algo}
  \caption{
  \textbf{$k$-means on \upmem{}.}
  The points are uniformly distributed across the DPUs.	
  Each DPU calculates the nearest cluster for each of its points.
  The host processor gathers the results, and computes the new centroids accordingly.
  }
  \label{fig:algo}
\end{figure}



%The Algorithm \ref{alg:kmeans} illustrates how the clustering process.
%In this algorithm, \(k\) is the number of clusters, \(data\) is the set of points, and \(\delta\) is a convergence criteria, as it uses an iterative technique.
%The Euclidean distance is referred as \(d\).
%
%\begin{algorithm}
%\begin{algorithmic}
%\Function{KMeans}{\(k\), \(data\), $\delta$}
%\State Distributes \(data\) across the DPUs
%
%\State Choose \( \tilde{C} := \left( \tilde{c_1} \ldots \tilde{c_k} \right) \) initial centroids
%\Repeat
%\State \( C = \tilde{C} \) 
%\State Send \(C\) to the DPUs
%\State \( R := \left(r_1 \ldots r_k \right) \)
%\State with \(r_i := \Call{kmean\_tasklet}{C, DPU_i} \)
%\State \( \tilde{C} = mean(r \in R) \)
%\Comment {Merges the result}
%\Until \( \vert \tilde{C} - C \vert \leq \delta \)
%\Comment {Convergence criteria}
%\State \Return \( \tilde{C} \)
%\Comment {Return the final centroids}
%\EndFunction
%
%\Function{kmean\_tasklet}{\(C\), $dpu$}
%\ForAll {point \( p \in get\_data\left(DPU\right) \)}
%	\State \( j := \argmin_i \, d(p, c_i) \)
%	\Comment {Find nearest cluster}
%	\State Assign \( p \) to cluster \( C_j \)
%\EndFor
%\ForAll {\( i\) in \( \{ 1 \ldots k \} \)}
%	\State \(c_i = mean(p \in C_i) \) \FDM{The new centroids are computed by the host I think}
%	\Comment {Compute new centroids}
%\EndFor
%\State \Return \(C\)
%\Comment {Return the final centroids}
%\EndFunction
%\end{algorithmic}
%\caption{K-Means algorithm on \upmem}
%\label{alg:kmeans}
%\end{algorithm}

Some improvements can be made to this simple algorithm.

\begin{itemize}
\item It is possible to begin the computation of the new centroids on the DPUs.
Each DPU can compute its version of the new centroids, as well as the number of points associated to it.
The processor can then compute, for each class, the exact mean by merging the partial centroids computed by the DPUs.
\item The amelioration proposed in \cite{arthur2007k} on the initial placement of the seeds can also be fairly easily implemented.
\item It is also possible to use the algorithms described in \cite{Fahim2006} or \cite{poteracs2014optimized} that both reduce the number of computations, 
but those would require small changes on the data structure.
\item The use of \(kd\)-trees as described in \cite{Kanungo:2002:EKC:628329.628801} is also possible, but it requires more profound changes to the data structure.
\end{itemize}

\subsection{Experimental Evaluation}
%Existing applications
Although the technology is not available yet, the performance benefit can be evaluated using the SDK and a cycle-accurate simulator provided by the company.
Experimentations from \cite{lavenier:hal-01294345, lavenier:hal-01327511} show that a 25x speed-up on data consuming genomics algorithms can be obtained using \upmem{} configuration.

To evaluate our implementation of the $k$-means algorithm on \upmem{}, we plan to compare it with both sequential and parallel existing implementations.
Performances will be evaluated on efficient architectures.
%TODO: precise the architecture, linux diststrib etc
%TODO: precise the \upmem configuration

Comparison will be made with the following implementations:

\begin{itemize}
 \item an optimized sequential version.
 \item MPI \cite{implemMPI}, a standardized message passing-system.
 \item CUDA \cite{implemCuda}, which permits to run general computations on the GPU.
 \item Hadoop \cite{implemHadoop}, a technology that uses MapReduce to provide distributed processing of large datasets.
\end{itemize}

\subsubsection{Datasets}

First, we would like to randomly generate several datasets,
each one with different characteristics.

Then, we plan to use real datasets.
We focus on datasets from the UCI Machine Learning Repository \cite{Lichman:2013} as it contains datasets for clustering with different aspects in terms of instances and attributes.
These datasets are also commonly used for experimental evaluation in the papers referred in section \ref{sec:related_kmeans}.

\subsubsection{Evaluation Metrics}

We consider two metrics to evaluate our implementation.

First, we use the Root Mean Squared Error (RMSE) to measure the quality of the clustering.
This metric evaluates how far the output is from the expected results.
If we denote by $C$ the results given by the algorithm we want to evaluate, and by $\widehat{C}$ what was expected, we have:

\[ \mathrm{MSE} = \frac{\sum\limits_{i=1}^k \left[ d(\widehat{c}_i, c_i) \right] ^2}{k} \hspace*{1cm} \mathrm{RMSE} = \sqrt{\mathrm{MSE}} \]


The second metric we use if \emph{SpeedUp}, to evaluate the runtime improvement.
If we denote by $RT_A$ (resp. $RT_B$) the runtime of the algorithm A (resp. B) with the same dataset, then we have:

\[ \emph{SpeedUp} = \frac{RT_B}{RT_A} \]

This metrics measures how fast the first implementation A is, compared with the second version B.
