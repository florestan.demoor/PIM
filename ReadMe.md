# PIM project

## COMPILATION
- make test_data: program to test the data structure
- make test_conf: program to test the configuration file parser 
- make main: the main k-means program
- make example_gen: program to generate datasets

## SCRIPTS
- checkRandom.sh
	runs the sequential and the PIM program and compares the results
- genConfig.sh <NB_DPUS> <NB_THREADS> <NB_CLUSTERS> <PATH> <DEST>
  	generates the configuration file and compile the dpu program
- runRandom.sh
	runs the PIM program and outputs the results
- runSeq.sh
	runs the sequential algorithm

## TODO
### General
- [x] Sending back cluster numbers and updating centroids in host
- [x] While loop in host program, mailbox message for host to tell dpu to resume
- [x] Using several DPUs, wait for all DPU before updating, partition points
- [x] Using several tasklets on each DPU
- [x] Using the data structures
- [x] Read the configuration from the configuration file in main.c (use the functions in conf.h)
- [x] Make a script to modify the config.json according to the configuration file and include this step in the compilation
- [x] Remove the useless mailboxes
- [x] kmeans++
- [x] script to run ./main brainlessly
- [x] BugFix => our score is worse than the sequential version
- [x] CAS basic version
- [x] how to compute time
- [x] Change all dirty&unsafe allocation inside stack for a beautiful&safe malloc/calloc <= approved by cferr & louis

- [ ] Enhance scripts [ ] => runSeq is not runnable independently
                      [ ] => genConf params is bugged
                      [ ] => Inconsistency: minfunc is runned inside runSeq
                             but not inside runRandom

### Opti => on the branch memory_optimization (the other one was to old so I created a new one)
- Bastien / Lesly => memory space/access optimization
    + [x] CPU part
    + [~] DPU part => change the mram_write_ll_256 to a more portable version
                   => Test, clean and merge
- Compare the two versions
- [ ] Suppr the data_t struct => Read points from the file and send them directly to the DPUs (find a way to make it work with the kmean ++ ?)
- [ ] Maybe we don't have to wait for all the DPUs termination to start processing one DPU
- [ ] Think about using threads inside the host ?



### Report
- [ ] Write a detailed report of the implementation
- [x] Make a schema of the DPU memory (with the optimization) so it is clear for everyone
