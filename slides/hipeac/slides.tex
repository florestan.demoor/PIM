\documentclass{beamer}
% \usepackage[utf8]{inputenc}
% \usepackage[T1]{fontenc}
% \usepackage[english]{babel}
% \usepackage{lmodern}
% \usepackage{anyfontsize}

\usepackage{appendixnumberbeamer}
\usepackage{hyperref}

\usepackage{tikz}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{algorithmicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{xcolor}
\usepackage{booktabs}

% \usepackage{subfig}

\usetheme[sectionpage=progressbar,subsectionpage=progressbar,numbering=fraction,
          progressbar=foot]{metropolis}
% \usecolortheme{owl}


\newcommand{\upmem}{\textsc{Upmem}}
\newcommand{\kmeans}{$k$-means}
\DeclareMathOperator{\argmin}{Argmin}

\graphicspath{{../../report/img/}}


\title{Evaluating a Processing-in-Memory Architecture with the \(k\)-means Algorithm}

% \subtitle{}

\date{\today}
\author{%
  Simon Bihel \hfill \href{mailto:simon.bihel@ens-rennes.fr}{\nolinkurl{simon.bihel@ens-rennes.fr}} \\
  Lesly-Ann Daniel \hfill \href{mailto:lesly-ann.daniel@ens-rennes.fr}{\nolinkurl{lesly-ann.daniel@ens-rennes.fr}} \\
  Florestan De Moor \hfill \href{mailto:florestan.de-moor@ens-rennes.fr}{\nolinkurl{florestan.de-moor@ens-rennes.fr}} \\
  Bastien Thomas \hfill \href{mailto:bastien.thomas@ens-rennes.fr}{\nolinkurl{bastien.thomas@ens-rennes.fr}}
}
\institute{%
  University of Rennes I \\
  École Normale Supérieure de Rennes
}
\titlegraphic{%
  \tikz[overlay,remember picture]
  \node[at= (current page.south east), anchor=south east] {%
    \includegraphics[height=1.5cm]{Universite_Rennes_1_logo.pdf} \hspace{5mm}
    \includegraphics[height=1.5cm]{Logo_ENS_Rennes.pdf}
  };
}

\begin{document}

\maketitle

\begin{frame}[plain,noframenumbering]{With Help From\dots}
  \author{Dominique Lavenier \hfill \url{dominique.lavenier@irisa.fr}}
  \usebeamertemplate*{author}
  \institute{CNRS \\ IRISA}
  \vspace*{-4mm}
  \usebeamertemplate*{institute}
  \tikz[overlay,remember picture]
  \node[at= (current page.east), anchor=east] {%
    \includegraphics[height=1.5cm]{CNRS_fr_quadri.pdf} \hspace{5mm}
    \includegraphics[height=1.5cm]{logo_irisa_cmjn-fond-transp.png}
  };

  \author{David Furodet \& the \upmem{} Team \hfill \url{dfurodet@upmem.com}}
  \usebeamertemplate*{author}
  \tikz[overlay,remember picture]
  \node[at= (current page.south east), anchor=south east] {%
    \includegraphics[height=1.5cm]{logo_upmem2.png}
  };
\end{frame}

\begin{frame}{Context}
  \begin{figure}
    \centering
    \includegraphics[scale=0.45]{intro}
  \end{figure}
\end{frame}

\begin{frame}{Table of contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}


\section{The \upmem{} Architecture}
\begin{frame}{\upmem{} architecture overview}
  \begin{figure}
    \centering
    \includegraphics[scale=0.60]{upmem_arch}
  \end{figure}
  %Legend
  \begin{description}
    \item[DPU] \textsc{dram} processing-unit
    \item[WRAM] execution memory for programs
    \item[MRAM] main memory
    \item[DIMM] dual in-line memory module
  \end{description}
\end{frame}

\begin{frame}{A massively parallel architecture}
  \begin{block}{Characteristics}
    \begin{itemize}
      \item Several DIMMs can be added to a CPU
      \item A 16 GBytes DIMM embeds 256 DPUs
      \item Each DPU can support up to 24 threads
    \end{itemize}
  \end{block}
  \vfill
  The context is switched between DPU threads every clock cycle.
  %Transition
  \pause%
  \vfill
  \textit{The programming approach has to consider this fine-grained parallelism.}
\end{frame}

\begin{frame}{\upmem{} Architecture Overview}
  On a programming level: two programs must be specified.
  \begin{figure}
    \hspace{-21cm}
    \begin{overlayarea}{0cm}{2cm}
      \includegraphics<1>[scale=0.7]{tasklet1}
      \includegraphics<2>[scale=0.7]{tasklet2}
    \end{overlayarea}
  \end{figure}
\end{frame}

% \begin{frame}{Communication}
%   \begin{columns}
%     \begin{column}{0.5\textwidth}
%       \textbf{MRAM}
%   %   \end{column}
%   %   \begin{column}<2->{0.5\textwidth}
%   %     \textbf{Mailboxes}
%     \end{column}
%   \end{columns}
%   \vfill
%   \begin{columns}
%     \begin{column}{0.5\textwidth}
%       \begin{figure}
%         \centering
%         \includegraphics[width=0.9\textwidth]{mram_com}
%       \end{figure}
%   %   \end{column}
%   %   \begin{column}<2->{0.5\textwidth}
%   %     \begin{figure}
%   %       \centering
%   %       \includegraphics[width=0.9\textwidth]{wram_com}
%   %     \end{figure}
%     \end{column}
%   \end{columns}
%   \vfill
%   \begin{columns}
%     \begin{column}{0.5\textwidth}
%       \begin{itemize}
%         \item Slow
%         \item Large amount of data
%       \end{itemize}
%   %   \end{column}
%   %   \begin{column}<2->{0.5\textwidth}
%   %     \begin{itemize}
%   %       \item Fast
%   %       \item Synchronization
%   %     \end{itemize}
%     \end{column}
%   \end{columns}
% \end{frame}

\begin{frame}{Drawbacks and advantages}
  \metroset{block=fill}
  \begin{block}{Drawbacks: computation power}
    \begin{itemize}
      \item Frequency around 750 MHz
      \item No floating point operations
      \item Significant multiplication overhead (no hardware multiplier)
      \item Explicit memory management
    \end{itemize}
  \end{block}
  \vfill
  \pause%
  \begin{block}{Advantages: data access}
    \begin{itemize}
      \item Parallelization power
      \item Minimum latency
      \item Increased bandwidth
      \item Reduced power consumption
    \end{itemize}
  \end{block}
  \vfill
  %Transition
  % \textit{Ideal for data-intensive operations such as the \kmeans{} algorithm.}
\end{frame}


\section{\texorpdfstring{$k$}{k}-means Implementation for the \upmem{} Architecture}
\begin{frame}{$k$-means Clustering Problem}
  \begin{block}{Partition data $\in \mathbb{R}^{n \times m}$ into $k$ clusters $C_1 \ldots C_k$}
    $n$ (resp.\ $m$): number of points (resp.\ attributes)

    $d$: Euclidean distance
    \begin{equation*}
      \argmin\limits_C \sum\limits_{i=1}^k \sum\limits_{p \in C_i} d(p, mean(C_i))
    \end{equation*}
  \end{block}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \textbf{Examples of applications}

      Segmentation

      Communities in social networks

      Market research

      Gene sequence analysis
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[scale=0.25]{kmeans_demo}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{$k$-means Standard Algorithm~\cite{lloyd1982least} }
  \begin{figure}
    \centering
    \begin{algorithmic}[1]
      \Function{$k$-means}{\(k\), data, $\delta$}
      \State\ Choose \( \tilde{C} := \left( \tilde{c_1} \ldots \tilde{c_k} \right) \) initial centroids
      \Repeat%
      \State\( C = \tilde{C} \)
      %\algemph{
      \ForAll{point \( p \in \)data}
      \State\textcolor{red}{\( j := \argmin_i \, d(p, c_i) \)}
      \Comment{Find nearest cluster}
      \State\textcolor{red}{Assign \( p \) to cluster \( C_j \)}
      \EndFor%
      %}
      \ForAll{\( i\) in \( \{ 1 \ldots k \} \)}
      \State\( \tilde{c_i} = mean(p \in C_i) \)
      \Comment{Compute new centroids}
      \EndFor%
      \Until\( \Vert \tilde{C} - C \Vert \leq \delta \)
      \Comment{Convergence criteria}
      \State\Return\( \tilde{C} \)
      \Comment{Return the final centroids}
      \EndFunction%
    \end{algorithmic}
    %\caption{$k$-means Standard Algorithm}
\label{alg:kmeans}
  \end{figure}
\end{frame}

\begin{frame}{\(k\)-means algorithm on \upmem{}}
  \begin{columns}
    \begin{column}{0.6\textwidth}
      \begin{figure}
        \centering
        \includegraphics[scale=0.8]{algo}
\label{alg:kmeans_on_upmem}
      \end{figure}
    \end{column}
    \begin{column}{0.4\textwidth}
      % \begin{center}
        The points are distributed across the DPUs.
      % \end{center}
    \end{column}
  \end{columns}
  % \vfill
  % %Transition
  % \pause%
  % \textit{Data parallelization of the standard algorithm, can it be improved?}
\end{frame}

\begin{frame}{Implementation \& Memory Management}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
        \item \texttt{int} type to store distance (easy to overflow with distances)
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{block}{MRAM}
        \begin{itemize}
          \item Global variables (e.g. \# of points)
          \item Centers
          \item Points
          \item New centers
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

% \begin{frame}{\kmeans{} enhancements}
% \end{frame}


\section{Experimental Evaluation}
\begin{frame}{Experimental Setup}
  \begin{block}{Simulator}
    \begin{itemize}
      \item Architecture not yet manufactured
      \item Cycle-Accurate simulator
    \end{itemize}
  \end{block}
  \vfill
  \pause%
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{block}{Datasets}
        \begin{itemize}
          \item \texttt{int}
          \item Randomly generated (not uniformly, with clusters)
        \end{itemize}
        \emph{Could not find ready-to-use integer large datasets.}
      \end{block}
    \end{column}
    \begin{column}{0.4\textwidth}
      \includegraphics[width=1.25\textwidth]{example_dataset_rng}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Number of Threads}
%   \begin{figure}
%     \begin{minipage}{.5\linewidth}
%       \centering
%       \subfloat[High number of points]{%
%         \includegraphics[scale=0.2]{threads_DATASET0}
%       }
%     \end{minipage}%
%     \begin{minipage}{.5\linewidth}
%       \centering
%       \subfloat[High dimensions]{%
%         \includegraphics[scale=0.2]{threads_DATASET1}
%       }
%     \end{minipage}\par\medskip
%     \centering
%     \vspace*{-5mm}
%     \subfloat[High number of centroids]{%
%       \includegraphics[scale=0.2]{threads_DATASET2}
%     }
% \label{img:threads}
%   \end{figure}
  \begin{columns}
    \begin{column}{0.7\textwidth}
      \hspace*{-1cm}
      \vspace*{1cm}
      \includegraphics[width=1.15\textwidth]{threads_all_without_y}
    \end{column}
    \begin{column}{0.3\textwidth}
      \metroset{block=fill}
      \begin{block}{High number of}
        \begin{itemize}
          \item points (N=1000000, D=10, K=5)
          \item dimensions (N=500000, D=34, K=3)
          \item centroids (N=100000, D=2, K=10)
        \end{itemize}
    \end{block}
    \end{column}
  \end{columns}
  \vfill
  \emph{Not the same runtime scales.}
\end{frame}

\begin{frame}{Number of DPUs}
  \begin{columns}
    \begin{column}{0.7\textwidth}
      \hspace*{-1cm}
      \vspace*{1cm}
      \includegraphics[width=1.15\textwidth]{dpus_DATASET1}
    \end{column}
    \begin{column}{0.3\textwidth}
      Always the same number of points.
    \end{column}
  \end{columns}
  \textbf{Time is divided by the number of DPUs.}
\end{frame}

\begin{frame}{Comparison with sequential \kmeans}
  \begin{table}
    \centering
    \begin{tabular}{p{0.45\textwidth}p{0.2\textwidth}p{0.2\textwidth}}
      \toprule
    Dataset & \multicolumn{2}{c}{Many \only<1>{\textbf{Points}}\only<2>{\textbf{Dimensions}}\only<3>{\textbf{Centers}}} \\
      Algorithm & 16-DPUs & 1 core SeqC \\
      \midrule
      Runtime (s) & \only<1>{1.568}\only<2>{4.534}\only<3>{0.4353} & \only<1>{0.268}\only<2>{0.119}\only<3>{0.0142} \\
      \textbf{Faster than SeqC with} & \multicolumn{2}{c}{\only<1>{\textbf{94}}\only<2>{\textbf{610}}\only<3>{\textbf{491}} \textbf{DPUs}} \\
      \bottomrule
    \end{tabular}
  \end{table}
  \vfill%
  \only<1>{\visible<2>{Large number of dimensions provides a large amount of multiplications to compute distances}}\only<2>{Large numbers of dimensions provides a large amount of multiplications to compute distances}\only<3>{Large numbers of centers provides a large amount of computation per memory transfer~\cite{Bender:2015:KCT:2818950.2818977}}
\end{frame}


\section*{Conclusion}
\begin{frame}{Conclusion}
  \begin{itemize}[<+->]
    \item Ideal use case with very low computation programs (e.g.\ genomic text processing~\cite{lavenier:hal-01294345,lavenier:hal-01327511})
    \item Even if there is no gain on time, power might be reduced
    \item Overflows when computing distances
    \item Implemented \kmeans{}++~\cite{arthur2007k} with GMP library (arbitrary precision numbers) but what was interesting is the time for an iteration
  \end{itemize}
\end{frame}

\begin{frame}{Going Further with the Hardware}
  \metroset{block=fill}
  \begin{block}{Actual Physical Device}
    \begin{itemize}
      \item Evaluate how the program behaves at large scale
      \item Impact on the DDR bus \& communications
    \end{itemize}
  \end{block}
  \pause%
  \begin{block}{Hardware Multiplication}
    \begin{itemize}
      \item Now: 40\% of multiplication instructions \& 30 instructions per multiplication
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Going Further with the \kmeans}
  \metroset{block=fill}
  \begin{block}{Keep the distance to the current nearest centroid~\cite{Fahim2006}}
    \textit{Easy to add in our implementation: keep distance in DPU}

    $+$ Avoid useless computations during next iteration

    $-$ Reduce number of points per DPU
  \end{block}
  \pause%
  \begin{block}{Define a border made of points that can switch cluster~\cite{poteracs2014optimized}}
    \textit{Harder to integrate}

    $+$ Reduce the number of distance computations

    $-$ Might involve the CPU
  \end{block}
  % \pause%
  % \begin{block}{Advanced data-structure for storing points (\(kd\)-trees)~\cite{Kanungo:2002:EKC:628329.628801} }
  %   \textit{Implementation possible, but with big modifications}

  %   $+$ Reduce the number of distance computations
  % \end{block}
\end{frame}


\appendix
\begin{frame}[standout]
  Thank You
\end{frame}


\section*{References}
\begin{frame}[allowframebreaks]{References}
  \bibliography{../../report/tex/biblio}
  \bibliographystyle{abbrv}
\end{frame}


\end{document}
