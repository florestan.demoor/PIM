# PIM foundation/theory
- [Terasis](http://www.ai.mit.edu/projects/aries/course/notes/terasys.pdf) PIMs in 95.

# PIM overview
- [EJL](http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=81C32BD1853A10FCC634B0AE65E2A142?doi=10.1.1.705.9293&rep=rep1&type=pdf) thermal feasibility of die-stacked PIM. Small overview on what's been done in the past.

# PIM with 3D die stacking
- [TOPPIM](http://www.dongpingzhang.com/wordpress/wp-content/uploads/2012/04/TOP-PIM-HPDC-paper.pdf) Comparison with GPUs, predicting performances. Benchmarks with examples of applications.

# PIM with logic implemented in DRAM processes
- [DIVA](http://www.isi.edu/~draper/papers/ics02.pdf)
- [ActivePages](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.34.9754&rep=rep1&type=pdf)
- [FlexRAM](http://s3.amazonaws.com/academia.edu.documents/37796400/iccd12.pdf?AWSAccessKeyId=AKIAJ56TQJRTWSMTNPEA&Expires=1475584335&Signature=GmpqvBu%2BjiKH6PMVxcUp%2BQdLxYQ%3D&response-content-disposition=inline%3B%20filename%3DFlexRAM_Toward_an_advanced_intelligent_m.pdf)

# PIM related architectures/applications
- [AYMC15](http://www.istc-cc.cmu.edu/publications/papers/2015/p336-ahn.pdf) proposed general architecture to integrate PIMs as e.g. cache with special low overhead instructions.
- [ISCA15](https://users.ece.cmu.edu/~omutlu/pub/tesseract-pim-architecture-for-graph-processing_isca15.pdf) application to parallel graph processing. Uses 3D stacking, section 3.1 explains the architecture.

# KMeans
- [POT14](https://fedcsis.org/proceedings/2014/pliks/258.pdf) proposes an optimized version of kmeans algorithm by avoiding computations for points that won't change cluster at next iteration
- [PAMI02](https://www.cs.umd.edu/~mount/Projects/KMeans/pami02.pdf) presents and evaluates an efficient kmeans algorithm using filtering
- [WKLIAO05](http://users.eecs.northwestern.edu/~wkliao/Kmeans/) parallel K-Means implementation in C and (OpenMP or MPI)
- [ZWMHQ09](http://link.springer.com/10.1007/978-3-642-10665-1_71) basic map-reduce implementation of k-means
- [ZHA11](http://www.jcomputers.us/vol8/jcp0801-02.pdf) MPI implementation ok kmeans
- [HUIM14](http://www.carch.ac.cn/~huimin/papers/CF.pdf) Collaborative divide-and-conquer kmeans algorithm
- [FAH06](http://www.zju.edu.cn/jzus/2006/A0610/A061002.pdf) efficient enhanced kmeans algorithm

For reference datasets:
- Random datasets with different probabilistic distributions
- Image Processing references (Lenna, etc)
- Recommendation systems datasets ? (high number of points and high dimension) (Movie Lens, Jester, MovieTweetings, etc)

---
# Slides
- [HRL](https://pdfs.semanticscholar.org/d4a7/a67228686975e882fee8d571f8257535e8b7.pdf) Heterogeneous Reconfigurable Logic (FPGA + CGRA)
- [PEI](https://users.ece.cmu.edu/~omutlu/pub/pim-enabled-instructons-for-low-overhead-pim_isca15-talk.pdf) [AYMC15]'s presentation
- [MR](http://wwwi10.lrr.in.tum.de/~weidendo/uchpc14/slides/kavi_uchpc14.pdf) Improving Node-level MapReduce Performance.

Key points to present PIMs technologies:
- many ways to implement PIMs, 3D-stacked DRAM is common these days but still lots of different implementation ([HRL] slide 4) and it's just part of the overall problem ([PEI] slide 4);
