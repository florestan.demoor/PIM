from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
from time import time

COLORS = ['r', 'b', 'g', 'yellow', 'purple']

N = 1000
D = 2

top = time()
X = np.random.normal((0, 0), 4, size=(N, D))
X2 = np.random.normal((20, -10), 6, size=(N, D))
X3 = np.random.normal((-18, 12), 5, size=(N, D))
X4 = np.random.normal((12, 14), 3, size=(N, D))
X5 = np.random.normal((-9, -16), 4, size=(N, D))
X = np.concatenate((X, X2))
X = np.concatenate((X, X3))
X = np.concatenate((X, X4))
X = np.concatenate((X, X5))
#X = X + (X - 5) * 3 + (X + 2) * 7
#top = time() - top

#print("KMeans computation took", top, "s")

kmeans = KMeans(n_clusters=5, random_state=0).fit(X)

L = kmeans.labels_

@np.vectorize
def mapColors(i):
  return COLORS[i]
  
mapColors = np.vectorize(mapColors)

C = mapColors(L)
plt.scatter(X[:,0], X[:,1], c=C, lw=0, s=5)

plt.savefig('kmeans_demo.svg', bbox_inches='tight')
